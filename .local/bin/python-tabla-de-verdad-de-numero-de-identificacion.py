#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 18 23:54:23 2025

@author: user
"""

def convertir_binario(numero):
    return bin(numero)[2:]

def generar_tabla_verdad(binario):
    print("Tabla de verdad de 4 entradas:")
    print("A | B | C | D | Q")
    print("-------------------------------------")
    contador = 0
    for a in [0, 1]:
        for b in [0, 1]:
            for c in [0, 1]:
                for d in [0, 1]:
                    try:
                        print(f"{a} | {b} | {c} | {d} | {binario[contador]}")
                        contador = contador + 1
                    except:
                        print(f"{a} | {b} | {c} | {d} | 0")

def main():
    numero = int(input("Ingrese un número entero: "))
    binario = convertir_binario(numero)
    print(f"El número {numero} en binario es: {binario}")

    generar_tabla_verdad(binario)

if __name__ == "__main__":
    main()
