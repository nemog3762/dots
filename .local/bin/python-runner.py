#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 14 17:29:28 2024

@author: user
"""

# %% Librerías

import subprocess
import os
import argparse

# %% Configuración de argumentos desde la línea de comandos

parser = argparse.ArgumentParser(description="Run SCRIPT n TIMES")

parser.add_argument(
    '--script',
    type=str,
    required=True,
    help="Nombre del script a ejecutar (con extensión .py)."
)

parser.add_argument(
    '--times',
    type=int,
    default=10,
    help="Número de veces que deseas ejecutar el script. (Por defecto: 10)"
)

parser.add_argument(
    '--verbose',
    type=bool,
    default=False,
    help="Show warnings or errors on execution (bool, default False)"
)

args = parser.parse_args()

# Asignar las variables a partir de los argumentos
script_a = args.script
numero_de_veces = args.times
verbose = args.verbose

# %% Main program

# Cambiar a directorio del script actual
# Esto lo tenía cuando ejecutaba el script desde Spyder
# os.chdir(os.path.dirname(os.path.abspath(__file__)))

# Verificar si el script a ejecutar existe
if not os.path.exists(script_a):
    print(f"Error: El script '{script_a}' no existe en el directorio actual.")
    exit(1)

# Print
print(f"Ejecutando {script_a} {numero_de_veces} veces...")

# Ejecutar el programa N veces
for i in range(numero_de_veces):
    print("*" * 30, f"{i + 1} de {numero_de_veces} ({(i + 1) / numero_de_veces * 100:.2f}%)")
    # result = subprocess.run(['python', script_a], capture_output=True, text=True)
    # Forzar que se usen "backends" no interactivos como Agg para que las gráficas no se muestren cuando se llaman desde acá
    result = subprocess.run(
    ['python', '-c', 'import matplotlib; matplotlib.use("Agg"); exec(open(r"{}").read())'.format(script_a)],
    capture_output=True,
    text=True
    )

    # Imprimir la salida del script ejecutado
    if verbose:
        print("*"*10)
        print("Salida del script")
        print(result.stdout)
        print("*"*10)
        print("Errores durante la ejecución")
        print(result.stderr)
