#!/bin/env bash

# Sources:
# https://lecorbeausvault.wordpress.com/2021/01/10/quickly-build-a-custom-bootable-installable-debian-live-iso-with-live-build/
# https://dquinton.github.io/debian-install/netinstall/live-build.html
# https://www.youtube.com/watch?v=idXrP2inpGs

# Vars
DIR="$HOME/build/debian-live"
STABLE_CODENAME="bookworm"
DIR_SKEL="config/includes.chroot_after_packages/etc/skel"
LIVE_ARCHITECTURE="amd64" # amd64 or i386

# Requirements
sudo apt install live-build -y

# Directory to store your live-build projects.
sudo rm -r "$DIR"
mkdir -p "$DIR"

# cd into the working directory
cd "$DIR" || exit 1

# Create the configuration files with 'lb config'.
lb config \
    --distribution "$STABLE_CODENAME" \
    --architecture "$LIVE_ARCHITECTURE" \
    --apt-recommends true \
    --debian-installer live \
    --debian-installer-distribution "$STABLE_CODENAME" \
    --debian-installer-gui false \
    --archive-areas "main contrib non-free-firmware" \
    --debootstrap-options "--variant=minbase" \
    --iso-application nemog3762 \
    --iso-preparer https://gitlab.com/nemog3762/the-one \
    --iso-publisher https://gitlab.com/nemog3762/the-one \
    --iso-volume nemog3762

# Customize the contents of the iso.
cat << EOF | tee config/package-lists/pkgs.list.chroot
xorg lightdm xterm
icewm
htop
zathura
gparted
lf
calamares calamares-settings-debian
EOF

# Copy dotfiles
mkdir -p "$DIR_SKEL"
cp -r "$HOME/gits/dots/." "$DIR_SKEL"
sudo rm -r "$DIR_SKEL"/.git
sudo rm -r "$DIR_SKEL"/.gitignore

# Create
respuesta="N"
read -p "Build? (y/N): " respuesta
[[ "$respuesta" == "y" ]] || exit 0
sudo lb build
