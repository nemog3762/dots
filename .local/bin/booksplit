#!/bin/bash

# Basado en la idea de Luke Smith:
# "Auto-ripping, splitting and tagging Albums and Audiobooks from YouTube"
# Fuente: https://www.youtube.com/watch?v=z_CcQhbwINU

# Requiere:
# apt install ffmpeg file

# TODO:
# Arreglar tildes :s
# Borrar de los nombres los :
# Reemplazar de los nombres las " por '

# En caso que no se ingresen 2 argumentos, mostrar ayuda
if [ $# -ne 2 ]
then
    echo "Uso: $(basename $0) <archivo_audio> <archivo_texto>"
    exit 1
fi

# Determinar el archivo de audio y de tiempos
MIME_1=$(file -b --mime-type "$1")
MIME_2=$(file -b --mime-type "$2")
if   [[ $MIME_1 == "audio/"* ]] && [[ $MIME_2 == "text/plain" ]]; then
    FILE_AUDIO="$1"
    FILE_TIMES="$2"
elif [[ $MIME_2 == "audio/"* ]] && [[ $MIME_1 == "text/plain" ]]; then
    FILE_AUDIO="$2"
    FILE_TIMES="$1"
else
    echo "Error, debe ser un archivo de audio y otro de texto plano"
    exit 1
fi

# Obtener tags
echo "Enter the artist/author:";    read -r TAG_ARTIST
echo "Enter the album/book title:"; read -r TAG_ALBUM
echo "Enter the year:";             read -r TAG_YEAR

# Determinar el directorio de salida, se inicia con una variable vacia
OUTPUT_DIR=""

# Si las variables tiene longitud no nula (es decir, esta definida y no vacia"
if [ -n "$TAG_YEAR" ]  && [ -n "$TAG_ALBUM" ]
then
    OUTPUT_DIR+="$TAG_YEAR $TAG_ALBUM"
fi

# Si la variable OUTPUT_DIR esta vacia, usar un formato predeterminado
if [ -z "$OUTPUT_DIR" ]
then
    # Valor por defecto
    OUTPUT_DIR="ALBUM_SPLIT_"$(date +'%Y-%m-%d-%H-%M-%S')""
fi

# Crear directorio de salida
mkdir -p "$OUTPUT_DIR" || { echo "Can't create directory?" && exit 1 ; }

# Contar el numero de lineas del archivo de texto
NUM_LINEAS="$(wc -l "$FILE_TIMES" | awk '{print $1}')"

# Por cada una de esas lineas...
for ((i = 1; i <= NUM_LINEAS; i++)); do

    # Obtener información de la linea actual
    LINEA_ACTUAL=$(sed -n "${i}p" "$FILE_TIMES")
    TIEMPO_INICIO=$(echo "$LINEA_ACTUAL" | awk '{print $1}')
    NOMBRE_CAP=$(echo "$LINEA_ACTUAL" | cut -d' ' -f2-)

    # Obtener información de la siguiente línea
    if [ $i != "$NUM_LINEAS" ]; then
        # Si no esta en la ultima linea...
        LINEA_SIGUIENTE=$(sed -n "$((i+1))p" "$FILE_TIMES")
        TIEMPO_FIN=$(echo "$LINEA_SIGUIENTE" | awk '{print $1}')
    else
        # Si esta en la ultima linea
        DURATION=$(ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 "$FILE_AUDIO")
        TIEMPO_FIN=$(date -u -d @"${DURATION}" +"%T")
    fi

    # Que el numero de pista tenga 2 digitos
    i_FORMATTED=$(printf "%02d" $i)

    # Crear copia fraccionada en la carpeta $OUTPUT_DIR con sus tags
    ffmpeg \
        -nostdin \
        -i "$FILE_AUDIO" \
        -ss "$TIEMPO_INICIO" \
        -to "$TIEMPO_FIN" \
        -c:a copy \
        -metadata title="$NOMBRE_CAP" \
        -metadata artist="$TAG_ARTIST" \
        -metadata album="$TAG_ALBUM" \
        -metadata track="$i_FORMATTED" \
        -metadata year="$TAG_YEAR" \
        "${OUTPUT_DIR}/${i_FORMATTED}${NOMBRE_CAP}".mp3
done
