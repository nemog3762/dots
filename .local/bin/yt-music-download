#!/bin/bash

### Based on (not the same) ###################################
### https://github.com/jordandalley/yt-dlp-scripts ############

### Script for downloading albums from Youtube Music ##########
### Usage: ./yt-music-album-download.sh <youtube music url> ###

# - Converts to MP3 from the best quality audio feed
# - Adds track number, album, artist, title, and release year into id3 tags
# - Adds album art embedded thumbnails

# Check dependencies
command -v yt-dlp >/dev/null || { echo "Install yt-dlp" && exit ; }
command -v jq     >/dev/null || { echo "Install jq"     && exit ; }

# Check url
[ -z "$1" ] && echo "I need a URL" && exit

# Downloading the json data of the first track only
echo "Retrieving album information..."
jsondata=`yt-dlp --dump-json --no-warnings --playlist-items 1 $1`

# Grabbing the "release_year" and "release_date" and comparing which is lowest integer.
# Sometimes Youtube Music doesn't even populate the "release_date" field, but when it does we need to compare it to "release_year"
# If both the "release_date" and "release_year" exist, check which one is the lower integer, and that should be the actual album release year.
jq_release_year_1=`echo $jsondata | jq --raw-output '.release_year'`
jq_release_date=`echo $jsondata   | jq --raw-output '.release_date'`
if [ $jq_release_date != 'null' ]; then
        jq_release_year_2=${jq_release_date::-4};
        year=$((jq_release_year_1<jq_release_year_2?jq_release_year_1:jq_release_year_2));
else
        year=$jq_release_year_1;
fi
echo Year: "$year"

# Grabbing the artist then removing any superfluous information after the first comma.
# Some artists put every band memember into the artist field.
jq_artist=`echo $jsondata | jq --raw-output '.artist'`
artist=${jq_artist%%,*}
echo Artist: "$artist"

# Album
jq_album=`echo $jsondata | jq --raw-output '.album'`
album=${jq_album%%,*}
album=$(echo ${album//\"/\'}) # Reemplazar " por ' en caso de tener en el título.
echo Album: "$album"

# Crear carpeta donde se guadarán las descargas
DIR="$artist/($year) $album"
mkdir --parents "$DIR" || exit 1
cd "$DIR"

# Pass to yt-dlp and begin download all the music!
yt-dlp  --ignore-errors \
        --no-warnings \
        --format "(bestaudio[acodec^=opus]/bestaudio)/best" \
        --extract-audio \
        --audio-format mp3 \
        --audio-quality 0 \
        --parse-metadata "playlist_index:%(track_number)s" \
        --parse-metadata ":(?P<webpage_url>)" \
        --parse-metadata ":(?P<synopsis>)" \
        --parse-metadata ":(?P<description>)" \
        --add-metadata \
        --postprocessor-args "-metadata date=\"${year}\" -metadata artist=\"${artist}\" -metadata album=\"${album}\"" \
        --embed-thumbnail \
        --ppa "EmbedThumbnail+ffmpeg_o:-c:v mjpeg -vf crop=\"'if(gt(ih,iw),iw,ih)':'if(gt(iw,ih),ih,iw)'\"" \
        --output "%(playlist_index)s %(title)s.%(ext)s" "$1"
