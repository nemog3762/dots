#!/usr/bin/env bash

# Verifica si se ha proporcionado un archivo PDF como argumento
[ -z "$1" ] && echo "Usage: $0 input.pdf" && exit 1

# Archivo de entrada PDF
INPUT_PDF="$1"

# Verifica si el archivo de entrada existe
[ ! -f "$INPUT_PDF" ] && echo "File not found: $INPUT_PDF" && exit 1

# Nombre base para los archivos de salida
OUTPUT_BASE="${INPUT_PDF%.*}_pag_"

# Convierte cada página del PDF a una imagen PNG
# convert -density 300 "$INPUT_PDF" "${OUTPUT_BASE}%03d.png"
# Convierte cada página del PDF a una imagen PNG
pagenum=1
while [ $pagenum -le $(pdfinfo "$INPUT_PDF" | awk '/Pages:/ {print $2}') ]
do
    convert -density 300 "$INPUT_PDF"[$(($pagenum - 1))] "${OUTPUT_BASE}$(printf "%03d" $pagenum).png"
    pagenum=$(($pagenum + 1))
done

# Verifica si la conversión fue exitosa
if [ $? -eq 0 ]; then
  echo "Se han creado las imágenes PNG de cada página del PDF correctamente."
else
  echo "Hubo un error al convertir las páginas del PDF a imágenes PNG."
  exit 1
fi

exit

[ -z "$1" ] && help-show "$(realpath "$0")" && exit

CMD=$1 && shift

FILE=$(basename -- "$1")
FILENAME="${FILE%.*}"
EXTENSION="${FILE##*.}"

case $CMD in
    'bookmarks')    ## Crear bookmars a PDF
        # TODO
        # https://www.xmodulo.com/add-bookmarks-pdf-document-linux.html?format=pdf
        [ ! -f "$1" ] && echo "file: $1 doesn't exist!" && exit
        ;;
    'fake_scan')    ## Crear efecto de pdf scaneado
        [ -f "$FILE" ] && exit
        convert \
            -density 300 \
            -depth 8 \
            -colorspace Gray "$FILE".pdf \
            -resize 200% \
            -blur 0x0.5 \
            -colorspace RGB "$FILENAME"_fake_scan.pdf
        ;;
    'png')          ## Crear png de pdf
        [ -f "$FILE" ] && exit
        # TODO: cambiar resolución como argumentos y el número de la página.
        _numero_hoja=1
        _resulucion_x="500"
        _resulucion_y="500"
        pdftoppm "$FILE" "$FILENAME_converted" \
            -png \
            -rx "$_resulucion_x" \
            -ry "$_resulucion_y" \
            -f "$_numero_hoja" \
            -singlefile
        ;;
    'title')        ## remane file to Title metadata

        # Guardar argumento
        respuesta="$2"

        # Función para limpiar el título del PDF
        clean_title() {
            echo "$1" | tr -cd '[:alnum:] ._-'
        }

        # Obtener el título del PDF usando pdfinfo
        title=$(pdfinfo "$FILE" | grep "Title:" | sed 's/Title:[[:space:]]*//')

        # Limpiar el título (eliminar caracteres no deseados)
        cleaned_title=$(clean_title "$title")

        # Si no encuentra el título, salir
        [ ! -n "$cleaned_title" ] && echo "No se pudo encontrar un título en el PDF." && exit 0

        # Mostrar información
        echo " "
        echo "Title (original): $FILE"
        echo " "
        echo "Title (new):      $cleaned_title.pdf"
        echo " "

        # Si $respuesta existe e igual a "y", entonces renombrar sin preguntar y salir
        [ "$respuesta" = "y" ] &&
            mv "$FILE" "$cleaned_title.pdf" &&
            echo "El archivo ha sido renombrado como: $cleaned_title.pdf" &&
            exit 0

        # Preguntar
        echo "¿Desea proceder con el renombrado del archivo? (y/N)" && read respuesta

        # Renombrar el archivo con el título limpio
        [ "$respuesta" = "y" ] &&
            mv "$FILE" "$cleaned_title.pdf" &&
            echo "El archivo ha sido renombrado como: $cleaned_title.pdf"
        ;;

    'title-new')        ## remane file to Title metadata

        # Función para limpiar el título del PDF
        clean() { echo "$1" | tr -cd '[:alnum:] ._-' ; }

        # Obtener el título del PDF usando pdfinfo
        title=$(pdfinfo "$FILE" | grep "Title:" | sed 's/Title:[[:space:]]*//')
        cleaned_author=$(pdfinfo "$FILE" | grep "Author:" | sed 's/Author:[[:space:]]*//')

        # Limpiar el título (eliminar caracteres no deseados)
        cleaned_title="$(clean "$title")"

        # Si no encuentra el título, salir
        [ ! -n "$cleaned_title" ] &&
            echo -e "\nNo se pudo encontrar un título en el PDF.\n" &&
            exit 1

        # Mostrar información
        echo " "
        echo "Title (original): $FILE"
        echo " "
        echo "Title (new):      $cleaned_title - $cleaned_author.pdf"
        echo " "
        echo "Rename [y/N]" && read respuesta

        # Si $respuesta existe e igual a "y", renombrar sin preguntar y salir
        [ "$respuesta" = "y" ] &&
            \mv "$FILE" "$cleaned_title - $cleaned_author.pdf" &&
            exit 0
        ;;
    *)
        help-show "$(realpath "$0")"
        ;;
esac
