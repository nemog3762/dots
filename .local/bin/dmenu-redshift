#!/bin/env bash

# Goal: Control redshift and picom

# To instantly adjust the color temperature of your screen use:
# $ redshift -P -O TEMPERATURE
# where TEMPERATURE is the desired color temperature (between 1000 and 25000).

restart()
{
    redshift -x
}

red()
{
    redshift -P -O 1000
}

blue()
{
    redshift -P -O 25000
}

xrandr_black_white()
{
    OUTPUT="$(xrandr | grep " connected " | awk '{print $1}')"
    [ -z "$OUTPUT" ] && exit
    xrandr --output "$OUTPUT" --brightness 0 --gamma 0.3:0.3:0.3
}

xrandr_restart()
{
    xgamma- -gamma 1.0
}

# https://github.com/kantord/compton-grayscale-reading-mode
compton_black_white()
{
#Define shader
GRAYSCALE=$(cat <<-END
uniform sampler2D tex;

void main() {
   vec4 c = texture2D(tex, gl_TexCoord[0].xy);
   float y = dot(c.rgb, vec3(0.299, 0.587, 0.114));
   vec4 gray = vec4(y, y, y, c.a);
   gl_FragColor = mix(c, gray, 0.95);

}
END
)

# Toggle compton with the selected mode
FILE="/tmp/compton-black-white"
killall -q compton
if [ ! -f "$FILE" ]; then
   compton "$@" --glx-fshader-win "$GRAYSCALE" --backend glx &
   touch "$FILE"
else
   compton "$@" &
   rm "$FILE"
fi
}

# If argument "$1"
[ "$1" == "compton_black_white" ] && compton_black_white && exit

# Select and eval option
OPTION="$(echo -e "restart\nred\nblue\ncompton_black_white" | dmenu -m 0 -i -p "Select option?")"
[ -z "$OPTION" ] && exit
eval "$OPTION"
