# sudo not required for some system commands
# cambios con emacs probando
for command in apt apt-add-repository ; do
	alias $command="sudo $command"
done; unset command

# Get 256 colors in xterm
TERM=xterm-256color

# Funciones
shopt -s autocd # cd without typing cd
bind 'set completion-ignore-case on' # cd is case insensitive

export TERMINAL="x-terminal-emulator"
export VISUAL="nvim"
nano --version 2&>/dev/null && export EDITOR="nano"
nvim --version 2&>/dev/null && export EDITOR="nvim"
emacs --version 2&>/dev/null && export EDITOR="emacs --no-window-system"

# Reload configs
alias so='source ~/.bashrc'
alias sx='xrdb ~/.Xresources'

# (u)xterm transparency
[ -n "$XTERM_VERSION" ] &&
    transset --version 2>/dev/null &&
    transset --id "$WINDOWID" 0.95 >/dev/null

# Import aliases
FILE="$HOME/.config/shell/aliases.txt" ; [ -f $FILE ] && . $FILE

# lf
LFCD="$HOME/.config/lf/lfcd.sh"
[ -f "$LFCD" ] && source "$LFCD" && alias l='lfcd' && bind '"\C-o":"lfcd\C-m"'  # bash
# Si hay iconos, cargarlos...
[ -f ~/.config/LF_ICONS ] && {
	LF_ICONS="$(tr '\n' ':' <~/.config/icons)" \
		&& export LF_ICONS
}

# fzf
FILE="/usr/share/doc/fzf/examples/key-bindings.bash" && [ -f $FILE ] && source $FILE

# PS1 https://bash-prompt-generator.org/
# https://stackoverflow.com/questions/1176386/automatically-timing-every-executed-command-and-show-in-bash-prompt
seconds2days() { # convert integer seconds to Ddays,HH:MM:SS
    printf "%ddays,%02d:%02d:%02d" $(((($1/60)/60)/24)) \
	   $(((($1/60)/60)%24)) $((($1/60)%60)) $(($1%60)) |
	sed 's/^1days/1day/;s/^0days,\(00:\)*//;s/^0//' ; }
trap 'SECONDS=0' DEBUG
PS1='\[\e[91m\]\W\[\e[0m\] \[\e[96m\]$?\[\e[0m\] ($(seconds2days $SECONDS)) \$ '

# Created by `pipx` on 2023-10-12 22:35:27
# export PATH="$PATH:/home/user/.local/bin"
[[ ":$PATH:" != *":/home/user/.local/bin:"*          ]] && export PATH="$PATH:/home/user/.local/bin"
[[ ":$PATH:" != *":/home/user/dotfiles/.local/bin:"* ]] && export PATH="$PATH:/home/user/dotfiles/.local/bin"

# If the .xsession-errors file is not a symbolic link, delete it and create it as such
# https://www.daniloaz.com/en/how-to-prevent-the-xsession-errors-file-from-growing-to-huge-size/
if [ ! -h $HOME/.xsession-errors ]; then
    /bin/rm $HOME/.xsession-errors
    ln -s /dev/null $HOME/.xsession-errors
fi

# man colors
# https://www.tecmint.com/view-colored-man-pages-in-linux/
# 0 – reset/normal
# 1 – bold
# 4 – underlined
# 31 – red
# 32 – green
# 33 – yellow
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

################################################################################
# Anaconda / Miniconda
################################################################################

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/user/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/user/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/user/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/user/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

# Función para cambiar entre entornos de Conda
function my-conda-env() {
    if [ -z "$1" ]; then
        echo "Uso: condaenv <nombre_del_entorno>"
        echo "Entornos disponibles:"
        conda env list | awk '{print $1}' | grep -v "^#"
    else
        # Desactivar el entorno actual, si hay uno activo
        if [[ -n "$CONDA_DEFAULT_ENV" ]]; then
            conda deactivate
        fi

        # Activar el entorno especificado
        conda activate "$1"
        if [ $? -eq 0 ]; then
            echo "Entorno activado: $1"
        else
            echo "Error: No se pudo activar el entorno '$1'. ¿Existe?"
        fi
    fi
}

# Activar mi env por defecto
conda activate njat
