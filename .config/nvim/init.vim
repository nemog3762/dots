let mapleader =","

" Verifica si estamos utilizando NeoVim (nvim) o Vim
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
" Comprueba si el archivo 'plug.vim' está ausente en el directorio de autoload
if empty(glob(data_dir . '/autoload/plug.vim'))
    " Si el archivo falta, descárgalo desde GitHub utilizando 'curl'
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    " Después de descargar 'plug.vim', se ejecuta PlugInstall al inicio de Vim
    " Esto instalará los plugins especificados en tu archivo de configuración
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.config/nvim_plugins/plugged')
    Plug 'dominikduda/vim_current_word'
    Plug 'preservim/nerdtree'
    Plug 'tpope/vim-commentary'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/fzf'
    " Barra
    " Plug 'vim-airline/vim-airline'
    " Ver colores
    Plug 'lilydjwg/colorizer'
    " Plug 'kovetskiy/sxhkd-vim'
    " CSV
    " Plug 'mechatroner/rainbow_csv'
    Plug 'chrisbra/csv.vim'
    " automatically recognizes file formats for various biological file formats.
    Plug 'biosyntax/biosyntax-vim'
    " Org
    Plug 'jceb/vim-orgmode'
    Plug 'tpope/vim-speeddating'
call plug#end()

" Plug 'dominikduda/vim_current_word'
    " Change highlight style of twins of word under cursor:
    " hi CurrentWordTwins guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
    hi CurrentWord      ctermbg=yellow ctermfg=black
    " hi CurrentWord guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
    hi CurrentWordTwins ctermbg=gray   ctermfg=white
" Plug vim-commentary
    " autocmd FileType apache setlocal commentstring=#\ %s
    nnoremap <F2> :Commentary<CR>
    vnoremap <F2> :Commentary<CR>
" Plug NERDTree
    nnoremap <LEADER>n :NERDTreeFocus<CR>
    nnoremap <C-n> :NERDTree<CR>
    nnoremap <C-t> :NERDTreeToggle<CR>
    nnoremap <C-f> :NERDTreeFind<CR>
" Plug fzf
    nnoremap <F3> :Maps<CR>
" Plug Goyo
    let g:goyo_height='80%'
    let g:goyo_width='80%'

" <++>
map  <LEADER><LEADER>      :keepp /<++><CR>ca<
imap <LEADER><LEADER> <esc>:keepp /<++><CR>ca<

" Configuración básica para Neovim
set title                       " Mostrar el título del archivo en la barra de título de la ventana
set go=a                        " Activar el modo de operaciones generales de comandos
set mouse=a                     " Habilitar el soporte del mouse en todos los modos
set clipboard+=unnamedplus      " Habilitar el portapapeles de sistema para copiar y pegar (sin tener que usar "+ o "*)
set splitbelow splitright       " Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
highlight Comment cterm=italic  " Colocar comentarios en cursiva
set autochdir                   " Change working directory to open buffer
" set showbreak=+++               " Wrap-broken line prefix
set colorcolumn=80

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Movimiento intuitivo
nnoremap j gj
nnoremap k gk
vnoremap j gj
vnoremap k gk

" GUI
set bg=light                 " Establecer el fondo a modo oscuro o claro
set virtualedit=all          " Habilitar el cursor de columna
set conceallevel=2

" Some basics:
" Mapeo para copiar la selección al portapapeles sin afectar el registro de yank
nnoremap c "_c
set nocompatible             " Desactivar el modo de compatibilidad con el Vim original
filetype plugin on           " Habilitar la carga automática de complementos según el tipo de archivo
syntax on                    " Habilitar el resaltado de sintaxis
set encoding=utf-8           " Establecer la codificación de caracteres a UTF-8
set number                   " Mostrar números de línea

" tabs / spaces
set tabstop=4               " number of columns occupied by a tab
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents

" Replace all is aliased to S.
nnoremap S :%s///g<LEFT><LEFT><LEFT>

" Automatically deletes all trailing whitespace and newlines at end of file on save. & reset cursor position
autocmd BufWritePre * let currPos = getpos(".")
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e " add trailing newline for ANSI C standard
autocmd BufWritePre * cal cursor(currPos[1], currPos[2])

" search
set showmatch               " Highlight matching brace
set ignorecase              " case insensitive
set hlsearch                " highlight search
set incsearch               " incremental search

" Desactivar el resaltado de búsqueda al presionar Esc
" nnoremap <Esc> :nohlsearch<CR><Esc>

" Habilitar Ctrl + S para guardar
nnoremap <C-S>      :w<CR>
imap     <C-S> <C-o>:w<CR>

" Habilitar Ctrl + F para buscar
nnoremap <C-f> /

" Activa la barra de estado y personaliza la apariencia
set laststatus=2

" Alternativas a ESC
inoremap jk <Esc>
inoremap kj <Esc>
inoremap JK <Esc>
inoremap KJ <Esc>

" Comentarios sin plugin
function! ToggleComment(comment_char)
    " Obtener la posición actual del cursor
    let cursor_pos = getpos('.')
    " Comprobar si la línea actual ya está comentada con el carácter dado
    if getline(".") =~ "^" . a:comment_char
        " Si está comentada, quitar los comentarios
        execute ".s/^" . a:comment_char . "//g"
    else
        " Si no está comentada, agregar los comentarios
        execute ".s/^/" . a:comment_char . "/g"
    endif
    " Restaurar la posición del cursor después de la operación
    call setpos('.', cursor_pos)
    if getline(".") =~ "^" . a:comment_char
        normal! 2l
    else
        normal! 2h
    endif
endfunction

" ortografía
map <LEADER>_spell_dual              :setlocal spell! spelllang=es,en<CR>
map <LEADER>_spell_spanish           :setlocal spell! spelllang=es<CR>
map <LEADER>_spell_off               :setlocal nospell<CR>
map <LEADER>_spell_next_wrong        ]s
map <LEADER>_spell_prev_wrong        ]s
map <LEADER>_spell_add_word_as_right zg
map <LEADER>_spell_add_word_as_wrong zw
map <LEADER>_spell_show_alternatives z=

" Open corresponding .pdf/.html or preview
map <LEADER>p :!xdg-open <c-r>%<backspace><backspace><backspace>pdf&<CR><CR>

" Ensure files are read as what I want:
autocmd BufRead,BufNewFile $HOME/.config/tux/*        set filetype=txt
autocmd BufRead,BufNewFile *.biom                     set filetype=json
autocmd BufRead,BufNewFile *.fish                     set filetype=bash
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man      set filetype=groff
autocmd BufRead,BufNewFile *.tex                      set filetype=tex
autocmd BufRead,BufNewFile *.txt,*.TXT                set filetype=org
autocmd BufRead,BufNewFile *.csv                      set nowrap
autocmd BufRead,BufNewFile $HOME/.emacs.d/abbrev_defs set filetype=lisp

" Avoid ctrl+z
nnoremap <c-z> :u<CR>      " Avoid using this**
inoremap <c-z> <c-o>:u<CR>

" Función personalizada que se ejecuta al presionar <LEADER><LEADER><LEADER>
function! FocusMode()
    set cursorline!
    set cursorcolumn!
    set number!

    " Opcional: Maximizar la ventana para un enfoque aún mayor
    execute 'resize ' . (winheight(0) + 70)

    " Opcional: Centrar el contenido verticalmente
    normal zz

    if &colorcolumn == '80'
        set colorcolumn=0
    else
        set colorcolumn=80
    endif
    if &laststatus == '2'
        set laststatus=0
    else
        set laststatus=2
    endif
endfunction
nnoremap <silent> <LEADER><LEADER><LEADER> :call FocusMode()<CR>

" pliegues
" set foldmethod=manual
set nofoldenable

" Jump to another file easily
" gf .............. jump to a file
" Ctrl-6 .......... jumps back
" gd ...............jump to local definitions
" gD ...............jump to global definitions
set hidden
set path+=**

" Edit your ~/.vimrc faster
set hidden
let mapLEADER=","
nnoremap <LEADER>v :e $MYVIMRC<CR>

" Reloads vimrc after saving but keep cursor position
if !exists('*ReloadVimrc')
   fun! ReloadVimrc()
       let save_cursor = getcurpos()
       source $MYVIMRC
       call setpos('.', save_cursor)
   endfun
endif
autocmd! BufWritePost $MYVIMRC call ReloadVimrc()

" listar los archivos en el directorio y escribirlo en el archivo
map <LEADER>ls :r!ls<CR>

" ortografía
map <LEADER>o :setlocal spell! spelllang=es<CR>

" funciones pensadas en usar con fzf
map <LEADER>_save :w!<CR>
map <LEADER>_save_as_temporal :w! /tmp/1.txt<CR>
map <LEADER>_sent :!sent "%"<CR>
map <LEADER>_toggle_Goyo :Goyo<CR>
map <LEADER>_toggle_wrap :set wrap!<CR>
map <LEADER>_toggle_cursorline  : set cursorline<CR>
map <LEADER>_toggle_cursorcolum : set cursorcolumn<CR>
map <LEADER>_toggle_colorcolumn : set colorcolumn=80<CR>
map <LEADER>_tab_next :tabn<CR>
map <LEADER>_tab_back :tabp<CR>
map <LEADER>_tab_new  :tabnew<CR>
map <LEADER>_center_cursor :au! VCenterCursor<CR>
map <LEADER>_json_format :%!jq .<CR>
map <LEADER>_csv_columns :%ArrangeColumn <CR>


let g:fzf_action = { 'enter': 'tab split' }

" Vim jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Reload syntax when the background changes
autocmd OptionSet background if exists("g:syntax_on") | syntax on | endif

function! HighlightRepeats() range
    let lineCounts = {}
    let lineNum = a:firstline
    while lineNum <= a:lastline
        let lineText = getline(lineNum)
            if lineText != ""
                let lineCounts[lineText] = (has_key(lineCounts, lineText) ? lineCounts[lineText] : 0) + 1
            endif
        let lineNum = lineNum + 1
    endwhile
    exe 'syn clear Repeat'
    for lineText in keys(lineCounts)
        if lineCounts[lineText] >= 2
            exe 'syn match Repeat "^' . escape(lineText, '".\^$*[]') . '$"'
        endif
    endfor
endfunction

command! -range=% HighlightRepeats <line1>,<line2>call HighlightRepeats()

map <LEADER>_highlight_repeats  :HighlightRepeats<CR>

"#### STATUSBAR ###############################################
if (&t_Co == 256 || &t_Co == 88) && !has('gui_running')
	set laststatus=2
	set noshowmode
	set statusline=
	set statusline+=%#DiffAdd#%{(mode()=='n')?'\ \ NORMAL\ ':''}
	set statusline+=%#DiffChange#%{(mode()=='i')?'\ \ INSERT\ ':''}
	set statusline+=%#DiffDelete#%{(mode()=='r')?'\ \ RPLACE\ ':''}
	set statusline+=%#Cursor#%{(mode()=='v')?'\ \ VISUAL\ ':''}
	set statusline+=\ %n\           			" buffer number
	set statusline+=%#Visual#       			" colour
	set statusline+=%{&paste?'\ PASTE\ ':''}
	set statusline+=%{&spell?'\ SPELL\ ':''}
	set statusline+=%#CursorIM#     			" colour
	set statusline+=%R                        	" readonly flag
	set statusline+=%M                        	" modified [+] flag
	set statusline+=%#Cursor#               	" colour
	set statusline+=%#CursorLine#     			" colour
	set statusline+=\ %t\                   	" short file name
	set statusline+=%=                          " right align
	set statusline+=%#CursorLine#   			" colour
	set statusline+=\ %Y\                   	" file type
	set statusline+=%#CursorIM#     			" colour
	set statusline+=\ %3l:%-2c\         		" line + column
	set statusline+=%#Cursor#       			" colour
	set statusline+=\ %3p%%\                	" percentage
	"##############################################################
endif

" ################################################## goyo
" map <LEADER>_toggle_Goyo :Goyo \| set bg=light \| set linebreak<CR>
function! s:goyo_enter()
  set linebreak
  " set spell spelllang=en_us
endfunction

function! s:goyo_leave()
  set nolinebreak
  set nospell
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()

" Working with CSV files
" Highlight a column in csv text.
" :Csv 1    " highlight first column
" :Csv 12   " highlight twelfth column
" :Csv 0    " switch off highlight
function! CSVH(colnr)
    if a:colnr > 1
        let n = a:colnr - 1
        execute 'match Keyword /^\([^,]*,\)\{'.n.'}\zs[^,]*/'
        execute 'normal! 0'.n.'f,'
    elseif a:colnr == 1
        match Keyword /^[^,]*/
        normal! 0
    else
        match
    endif
endfunction
command! -nargs=1 Csv :call CSVH(<args>)
