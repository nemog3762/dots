# vim: ft=config

###############################################################################
# Basic vars

# icons
set icons true
# set internal field separator (IFS) to "\n" for shell commands
set ifs "\n"
#
set period 1
# leave some space at the top and the bottom of the screen
set scrolloff 5
# Enable Borders
# set drawbox true
# Changing the preview directory cursor
# set cursorpreviewfmt ""
set incsearch true
set incfilter true
set dircounts true

###############################################################################
# dragon - drag and drop

map ,
map ,, &           dragon --and-exit --all --on-top $fx
# map , &           dragon-help dd $fx
# map ; $           dragon-help fzf
# map ; $ setsid -f dragon            --all --on-top $(find -maxdepth 1 | fzf -m)

###############################################################################
# preview

set previewer   ~/.config/lf/preview-ueberzug
set cleaner     ~/.config/lf/cleaner
set previewer   ~/.config/lf/preview
cmd toggle-ueberzug &{{
    [ ! -f /tmp/lf-ueberzug-$id ] && touch /tmp/lf-ueberzug-$id || rm /tmp/lf-ueberzug-$id
    if [   -f /tmp/lf-ueberzug-$id ]; then
        lf -remote "send $id :set previewer ~/.config/lf/preview-ueberzug"
        lf -remote "send $id reload"
        lf -remote "send $id echo Ueberzug ✅✅✅"
    else
        lf -remote "send $id :set previewer ~/.config/lf/preview"
        lf -remote "send $id reload"
        lf -remote "send $id echo Ueberzug ❌❌❌"
    fi
}}
map . toggle-ueberzug

###############################################################################
# apariencia

map z1 &lf -remote "send $id :set preview false; set ratios 1; set info size"
map z2 &lf -remote "send $id :set preview true;  set ratios 1:2"
map z3 &lf -remote "send $id :set preview false; set ratios 1:2"
map z4 &lf -remote "send $id :set preview true;  set ratios 1:4"
map z5 &lf -remote "send $id :set preview false; set ratios 1:4"
map z6 &lf -remote "send $id :set preview true;  set ratios 1:2:3"
map z7 &lf -remote "send $id :set preview false; set ratios 1:2:3"
map zz $lf -remote "send $id :set preview true;  set ratios 1:2"
set preview true
set ratios 1:2

###############################################################################
# rename

map <f-2> rename
map I :rename; cmd-home
map A :rename; cmd-end
map C :rename; cmd-delete-home
#map C :rename; cmd-end; cmd-delete-home
map b $vidir $(basename $fs)

###############################################################################
# move

map g. cd ~/gits/
map g1 cd "~/Google Drive"
map ge cd "~/.emacs.d/"
map g2 cd "~/Google Drive/svm_ezkernel"
map g3 cd ~/OneDrive
map g? $lf -doc | less
map gc cd ~/.config
map gL cd ~/.local
map gM cd /media/user/
map gd cd ~/Documentos
map gi cd ~/Imágenes
map gl %lf -remote "send ${id} select '$(readlink $f)'"
map gm cd ~/Música
map go cd ~/Descargas
map gq cd ~/Descargas/queue
map gr cd "~/Google Drive/Roms"
map gv cd ~/Vídeos

# Move to next/previous sibling directory
map } :updir; set dironly true; down; set dironly false; open
map { :updir; set dironly true; up;   set dironly false; open

###############################################################################
# GUI

map n

# open file manager gui
map ng & xdg-open .

# scripts
map nas ! adb-help push "$f"
map nag ! adb-help pull
map nfv ! ffmpeg-help video-convert-x264 "$f"
map npt ! pdf-help title "$f"

# devour - no funciona en tmux
map ñ $ devour xdg-open "$f"
# map ñ & editor-help "$f"

###############################################################################
# Open

map E $emacs -nw "$f"

map <enter> open

cmd open &{{
    # Abrir por extensión
    case "$f" in
        *.gdsheet|*.gddoc|*.gdsheet)
            insync open-cloud "$f"
            ;;
    esac
    # Abrir por mimetype
    case $(file --mime-type "$f" -bL) in
        # image/*) rotdir $f | grep -i "\.\(png\|jpg\|jpeg\|gif\|webp\|avif\|tif\|ico\)\(_large\)*$" |
        #     setsid -f nsxiv -aio 2>/dev/null | while read -r file; do
        #         [ -z "$file" ] && continue
        #         lf -remote "send select \"$file\""
        #         lf -remote "send toggle"
        #     done &
        #     ;;
        # text/*|application/json)
        #     $EDITOR "$f"
        #     ;;
        *)
            setsid -f xdg-open "$f" >/dev/null 2>&1
            ;;
    esac
}}

###############################################################################
# comandos

map @ push :$<space>"$f"<left><left><left><left><left><left>

###############################################################################
# compress

map net extract-to
map neH & engrampa --extract-here "$f" ; lf -remote 'send reload'
map ncc & engrampa --add          "$f" ; lf -remote 'send reload'
map neh $ extract                 "$f" ; lf -remote 'send reload'
map ncz $ compress  zip "$f"
map nc7 $ compress 7zip "$f"

cmd extract-to ${{
	clear;
	dest="$(find $HOME -type d | fzf --prompt='Extract to: ')"
    [ -z "$dest" ]&& return 0
	# setsid -f engrampa --extract-to=$dest "$f" 2&>/dev/null &
	engrampa --extract-to=$dest "$f"
    # ex "$f" -d "$dest"
}}

###############################################################################
# trash

map DDD                      trash
map <delete><delete><delete> trash

cmd trash &{{
	set -f
	if hash trash-put 2>/dev/null; then
		trash-put -v -- $fx
	else
		mkdir -p "$HOME"/.trash

	fi
}}

###############################################################################
# Mouse

set mouse true
map <m-1> updir  # primary
map <m-2> open   # secondary
# map <m-3> down  # middle
# map <m-down> down
# map <m-up>   up
# map <m-2>    open

###############################################################################
# copy / paste / link

map y
map p
map yy copy
map pp paste
map pl link-soft
map ph link-hard

cmd link-soft %{{
    set -- $(cat ~/.local/share/lf/files)
    mode="$1"
    shift
    if [ "$#" -lt 1 ]; then
        lf -remote "send $id echo no files to link"
        exit 0
    fi
    # symbolically copy mode is indicating a soft link
    ln -sr -t . -- "$@"
    rm ~/.local/share/lf/files
    lf -remote "send clear"
}}

cmd link-hard %{{
    set -- $(cat ~/.local/share/lf/files)
    mode="$1"
    shift
    if [ "$#" -lt 1 ]; then
        lf -remote "send $id echo no files to link"
        exit 0
    fi
    # while a move mode is indicating a hard link
    ln -t . -- "$@"
    rm ~/.local/share/lf/files
    lf -remote "send clear"
}}

###############################################################################
# Command mode

cmap <tab>     cmd-menu-complete
cmap <backtab> cmd-menu-complete-back
cmap <up>      cmd-history-prev
cmap <down>    cmd-history-next

###############################################################################
# fzf

map <f-5> $ fzf-help open "$f"
map ,j jumpto
# Select
map ,f $lf -remote "send $id select \"$(find .           | fzf -i)\""
# Select in actual directory
map ,g $lf -remote "send $id select \"$(find -maxdepth 1 | fzf)\""
# Select and jumpand jump  subdirectory of current directory
map gj $lf -remote "send $id select \"$(find . -type d | fzf)\""
# ripgrep
map gs $ lf -remote "send $id select \"$(fzf-help search)\""

cmd fzf-move-to ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear;
	dest="$(find $HOME -type d | fzf --prompt='Move to? ')" &&
	for x in $fx; do
		eval mv -iv \"$x\" \"$dest\"
	done &&
	notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd fzf-move-inside ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear;
	dest="$(find . -type d | fzf --prompt='Move to? ')" &&
	for x in $fx; do
		eval mv -iv \"$x\" \"$dest\"
	done &&
	notify-send "🚚 File(s) moved." "File(s) moved to $dest."
}}

cmd fzf-copy-to ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear;
	dest="$(find $HOME -type d | fzf --prompt='Copy to? ')" &&
	for x in $fx; do
		eval cp -ivr \"$x\" \"$dest\"
	done &&
	notify-send "📋 File(s) copied." "File(s) copies to $dest."
}}

cmd jumpto ${{
	clear; tput cup $(($(tput lines)/3)); tput bold
	set -f
	clear;
	dest="$(find $HOME -type d | fzf --prompt='Jump to? ')" &&
    lf -remote "send $id cd \"$dest\""
}}

cmd jump_to_tag ${{
    # Archivos de TAGS
    FILE="$HOME/.local/share/lf/tags"
    # Determinar número de líneas
    FILE_LINES="$(wc -l $FILE | awk '{print $1}')"
    # Si tiene 0 líneas, entonces no hay archivos marcados
    [ $FILE_LINES -eq 0 ] && return 0
    # Seleccionar DEST o salir
    DEST="$(cat $FILE | fzf --prompt='Go to?')" || return 0
    # Limipar DEST
    DEST="$(echo $DEST | sed 's/\:\*$//g')"
    # Si DEST es archivo, seleccionar, si es directorio, entrar
    [ -d "$DEST" ] && lf -remote "send $id cd \"$DEST\""
    [ -f "$DEST" ] && lf -remote "send $id select \"$DEST\""
}}

map gt jump_to_tag

###############################################################################
# otros

# tecla retroceso
map <backspace2> updir

# new terminal
cmd new-terminal & setsid -f x-terminal-emulator . &

# New dir
cmd new-dir  &mkdir -p "$(echo $* | tr ' ' '\ ')"
map nd push :new-dir<space>

# New text
cmd new-file &touch "$(echo $* | tr ' ' '\ ')"
map nt push :new-file<space>

# ocultar / mostrar archivos
map ,h set hidden!

# cambiar permisos de ejecución
map x &chmod -x $fx
map X &chmod +x $fx

# directory size
cmd size_file %{{
    [   -L $f ] && lf -remote "send echoerr $(du -shL $f)"
    [ ! -L $f ] && lf -remote "send echoerr $(du -sh  $f)"
}}
map ¿ size_file

map F filter

###############################################################################

# tmux

# map s| &tmux split-window -h lf
# map s- &tmux split-window -v lf

################################################################################
# on-cd
cmd on-cd &{{
    # Crear archivo donde se guardarán carpetas visitadas
        FILE=/tmp/lf-on-cd-$id
        touch $FILE
    # Escribir en $FILE la carpeta visitada
        pwd >> $FILE
}}

cmd history &{{
    # cd en carpeta anterior a la última
        FILE=/tmp/lf-on-cd-$id
        DEST="$(cat $FILE | tail -n 2 | head -n 1)"
        lf -remote "send $id cd \"$DEST\""
}}

cmd history_fzf ${{
    # pasar el historial de carpetas por fzf y escoger una opción
        FILE=/tmp/lf-on-cd-$id
        DEST="$(cat $FILE | fzf -i)"
        lf -remote "send $id cd \"$DEST\""
}}

on-cd

map gb history
map <f-3> history_fzf

############################
# Ver mimetype

cmd mime_change ${{
    # Determinar el mimetype del archivo seleccionado
    VAR_TIPO="$(mimetype --brief "$f")"

    # listar archivos .desktop
    ls /usr/share/applications         >  /tmp/1.txt
    ls $HOME/.local/share/applications >> /tmp/1.txt

    # Escoger un .desktop
    VAR_DESKTOP="$(cat /tmp/1.txt | fzf)"
    [ -z $VAR_DESKTOP ] && exit

    echo xdg-mime default  "$VAR_DESKTOP" "$VAR_TIPO"

    xdg-mime default  "$VAR_DESKTOP" "$VAR_TIPO"
}}

cmd mime_view !{{
    # Determinar el mimetype del archivo seleccionado
    VAR_TIPO="$(mimetype --brief "$f")"

    # Determinar con que .desktop se abre el archivo
    VAR_DEFAULT="$(xdg-mime query default "$VAR_TIPO")"

    echo " "
    echo "Tipo de archivo:        $VAR_TIPO"
    echo "Aplicación por defecto: $VAR_DEFAULT"
    echo " "
}}

map O mime_change
map o mime_view

############################
# on quit
cmd on-quit ${{
    # Remover archivo tmp de lf-on-cd
        FILE=/tmp/lf-on-cd-$id
        [ -f $FILE ] && \rm $FILE 2>/dev/null
    # Remover archivo tmp de lf-ueberzug
        FILE=/tmp/lf-ueberzug-$id
        [ -f $FILE ] && \rm $FILE 2>/dev/null
    # Eliminar repetidos de $USER/.local/share/lf/history
        FILE="$HOME/.local/share/lf/history"
        TEMP="/tmp/history"
        [ -f "$FILE" ] &&
            sort "$FILE" | uniq > "$TEMP" && rm "$FILE" && mv "$TEMP" "$FILE"
    # Salir
        lf -remote "send $id quit"
}}

map q
map q on-quit
