# vim: filetype=config

default_orientation vertical
workspace_auto_back_and_forth yes

# Configuración de apariencia de ventanas
    for_window [class=".*"] border pixel 4

# Variables
    set $mod1 Mod1
    set $mod  Mod4

# Define names for default workspaces
    set $ws1 "1"
    set $ws2 "2"
    set $ws3 "3"
    set $ws4 "4"
    set $ws5 "5"
    set $ws6 "6"
    set $ws7 "7"
    set $ws8 "8"
    set $ws9 "9"

    # colores
    set $yellow  #ffff00
    set $blue    #0000ff
    set $red     #ff0000
    set $green   #00ff00
    set $magenta #ff00ff

# class                 border      bground     text    indicator child_border
client.focused          $blue     $yellow       $red    $green    $red
# client.focused_inactive #333333 #5F676A #FFFFFF #484E50   #5F676A
# client.unfocused        #333333 #222222 #888888 #292D2E   #222222
# client.urgent           #2F343A #900000 #FFFFFF #900000   #900000
# client.placeholder      #000000 #0C0C0C #FFFFFF #000000   #0C0C0C

# Font for window titles.
    font pango:monospace 8
    # font pango:DejaVu Sans Mono 8

# Autostart
    # Start XDG autostart .desktop files using dex. See also https://wiki.archlinux.org/index.php/XDG_Autostart
    exec --no-startup-id dex --autostart --environment i3

    # The combination of xss-lock, nm-applet and pactl is a popular choice, so
    # they are included here as an example. Modify as you see fit.

    # xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
    # screen before suspend. Use loginctl lock-session to lock your screen.
    exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

    # NetworkManager is the most popular way to manage wireless networks on Linux,
    # and nm-applet is a desktop environment-independent system tray GUI for it.
    exec --no-startup-id nm-applet

    # i3 master stack
    # exec --no-startup-id  $HOME/.config/i3/i3-master-stack/i3_master

    exec --no-startup-id sxhkd
    exec --no-startup-id nitrogen --restore

# Use pactl to adjust volume in PulseAudio.
    # set $refresh_i3status killall -SIGUSR1 i3status
    set $refresh_i3status exec pkill -SIGRTMIN+12 i3blocks
    bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@   +2%    && $refresh_i3status
    bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@   -2%    && $refresh_i3status
    bindsym XF86AudioMute exec        --no-startup-id pactl set-sink-mute   @DEFAULT_SINK@   toggle && $refresh_i3status
    bindsym XF86AudioMicMute exec     --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# Use Mouse+$mod to drag floating windows to their wanted position
    floating_modifier mod1

# move tiling windows via drag & drop by left-clicking into the title bar,
# or left-clicking anywhere into the window while holding the floating modifier.
    tiling_drag modifier titlebar

# reglas de ventanas
    for_window [class="Pinentry"] sticky enable
    for_window [class="sent"] border pixel 0px
    for_window [window_role="GtkFileChooserDialog"] resize set 800 600
    for_window [window_role="GtkFileChooserDialog"] move position center
    for_window [title="Default - Wine desktop"] floating enable
    for_window [class="Keepassx"] floating enable
    for_window [class="Lxappearance"] floating enable
    # $ws2
        # for_window [class="Firefox-esr"]    move workspace $ws2
    # $ws5
        for_window [title="GIMP Startup"]   move workspace $ws5
        for_window [class="Gimp"]           move workspace $ws5
    # General
        # Bindings to make the webcam float and stick.
        for_window [title="video0 - mpv"] floating enable
        for_window [title="video0 - mpv"] sticky enable
        for_window [title="video0 - mpv"] border pixel 2
        no_focus   [title="video0 - mpv"]

# Fila FN
    bindsym $mod+F2	exec --no-startup-id window-flash
    bindsym $mod1+F4 kill

# Fila 0
    bindsym $mod+bar                split h
    bindsym $mod+1                  workspace                   number $ws1
    bindsym $mod+Shift+1            move container to workspace number $ws1
    bindsym $mod+2                  workspace                   number $ws2
    bindsym $mod+Shift+2            move container to workspace number $ws2
    bindsym $mod+3                  workspace                   number $ws3
    bindsym $mod+Shift+3            move container to workspace number $ws3
    bindsym $mod+4                  workspace                   number $ws4
    bindsym $mod+Shift+4            move container to workspace number $ws4
    bindsym $mod+5                  workspace                   number $ws5
    bindsym $mod+Shift+5            move container to workspace number $ws5
    bindsym $mod+6                  workspace                   number $ws6
    bindsym $mod+Shift+6            move container to workspace number $ws6
    bindsym $mod+7                  workspace                   number $ws7
    bindsym $mod+Shift+7            move container to workspace number $ws7
    bindsym $mod+8                  workspace                   number $ws8
    bindsym $mod+Shift+8            move container to workspace number $ws8
    bindsym $mod+9                  workspace                   number $ws9
    bindsym $mod+Shift+9            move container to workspace number $ws9
    bindsym $mod+BackSpace          exec --no-startup-id x-terminal-emulator -e system-act

# Fila 1
    # Bind alt+tab to switch windows in current workspace only.
    bindsym $mod1+Tab               exec --no-startup-id $HOME/.config/i3/i3-alt-tab.py/i3-alt-tab.py next current
    bindsym $mod1+Shift+Tab         exec --no-startup-id $HOME/.config/i3/i3-alt-tab.py/i3-alt-tab.py prev current
    # Bind alt+tab to switch windows on all workspaces.
    # bindsym $mod1+Tab               exec --no-startup-id $HOME/.config/i3/i3-alt-tab.py/i3-alt-tab.py next all
    # bindsym $mod1+Shift+Tab         exec --no-startup-id $HOME/.config/i3/i3-alt-tab.py/i3-alt-tab.py prev all
    bindsym $mod+Tab                workspace next
    bindsym $mod+Shift+Tab          workspace prev
    bindsym $mod+Shift+t            sticky toggle
    bindsym $mod+Shift+q            kill
    bindsym $mod+Shift+e            exec "i3-nagbar -t warning -m 'Exit i3?' -B 'Yes, exit i3' 'i3-msg exit'"
    # bindsym $mod+r                  mode "resize"
    bindsym $mod+Shift+r            restart
    bindsym $mod+u                  [title="dropdown"] scratchpad show; [title="dropdown"] move position center
    bindsym $mod+dead_acute         resize grow height 1 px or 1 ppt
    bindsym $mod+Shift+dead_acute   resize grow height 10 px or 10 ppt
    bindsym $mod+plus               resize shrink   height 1 px or 1 ppt
    bindsym $mod+Shift+plus         resize shrink   height 10 px or 10 ppt

# Fila 2
    bindsym --release Caps_Lock     exec pkill -SIGRTMIN+11 i3blocks
    bindsym --release Num_Lock      exec pkill -SIGRTMIN+11 i3blocks
    # bindsym $mod+a                  exec --no-startup-id screenshot a
    bindsym $mod+d                  exec --no-startup-id dmenu_run
    # bindsym $mod+f                  exec --no-startup-id screenshot f
    bindsym $mod+Shift+f            fullscreen toggle
    bindsym $mod+Shift+g            mode "$mode_gaps"
    bindsym $mod+h                  focus left
    bindsym $mod+Shift+h            move  left
    bindsym $mod+j                  focus down
    bindsym $mod+Shift+j            move  down
    bindsym $mod+k                  focus up
    bindsym $mod+Shift+k            move  up
    bindsym $mod+l                  focus right
    bindsym $mod+Shift+l            move  right
    bindsym $mod1+Ctrl+l            exec --no-startup-id i3lock -c 000000
    bindsym $mod+braceleft          resize grow   width 10 px or 10 ppt
    bindsym $mod+Shift+braceleft    resize grow   width 1  px or 1  ppt
    bindsym $mod+braceright         resize shrink width 10 px or 10 ppt
    bindsym $mod+Shift+braceright   resize shrink width 1  px or 1  ppt

# Fila 3
    bindsym $mod+greater            move workspace to output right
    bindsym $mod+less               move workspace to output left
    # bindsym $mod+x                  exec --no-startup-id rofi-run
    bindsym $mod+Shift+c            reload
    bindsym $mod+v split            toggle
    bindsym $mod+Shift+v            layout toggle split
    bindsym $mod+b                  mode "$mode_layout"
    bindsym $mod+n                  [title="dropdown_terminal"] scratchpad show; [title="dropdown_terminal"] move position center
    bindsym $mod+comma              focus parent
    bindsym $mod+period             focus child
    bindsym $mod+minus              split v

# Teclas flechas
    bindsym $mod+Shift+Return       exec i3-sensible-terminal
    bindsym $mod+Left               focus left
    bindsym $mod+Shift+Left         move  left
    bindsym $mod+Down               focus down
    bindsym $mod+Shift+Down         move  down
    bindsym $mod+Up                 focus up
    bindsym $mod+Shift+Up           move  up
    bindsym $mod+Right              focus right
    bindsym $mod+Shift+Right        move  right
    bindsym $mod+space              focus mode_toggle
    bindsym $mod+Shift+space        floating toggle
    bindsym Mod1+Ctrl+Right         workspace next
    bindsym Mod1+Ctrl+Left          workspace prev

# For screenshots and recording
    bindsym Print 			        exec --no-startup-id screenshot f
    bindsym Shift+Print 	        exec --no-startup-id screenshot a
    bindsym Mod1+Print 		        exec --no-startup-id screenshot w

set $mode_layout Layout: (t)abbed, (s)tacked, t(o)ggle split
    mode "$mode_layout" {
        bindsym s      layout stacking     ; mode "default"
        bindsym t      layout tabbed       ; mode "default"
        bindsym o      layout toggle split ; mode "default"
        bindsym Return mode "default"
        bindsym Escape mode "default"
    }

bar {
    font pango:DejaVu Sans Mono 10
    position top
    status_command i3blocks
    tray_output primary
}

#dropdown
    for_window [title="dropdown"] floating enable
    for_window [title="dropdown"] resize set 625 400
    for_window [title="dropdown"] move scratchpad
    for_window [title="dropdown"] border pixel 5
    exec --no-startup-id x-terminal-emulator -T dropdown -e R -q

#dropdown_terminal
    for_window [title="dropdown_terminal"] floating enable
    for_window [title="dropdown_terminal"] resize set 800 500
    for_window [title="dropdown_terminal"] move scratchpad
    for_window [title="dropdown_terminal"] border pixel 5
    exec --no-startup-id x-terminal-emulator -T dropdown_terminal

# Gaps
    # https://lottalinuxlinks.com/how-to-build-and-install-i3-gaps-on-debian/

    set $gaps_inner 20
    set $gaps_outer 5

    gaps inner $gaps_inner
    gaps outer $gaps_outer

    # Only enable gaps on a workspace when there is at least one container
    # smart_gaps on

    # Activate smart borders (always)
    #smart_borders on

    # Activate smart borders (only when there are effectively no gaps)
    #smart_borders no_gaps

    # Hide edge borders only if there is one window with no gaps
    hide_edge_borders smart_no_gaps

    set $mode_gaps Gaps: (o) outer, (i) inner, (r) remove all gaps, (d) default
    set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
    set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)

    mode "$mode_gaps" {
            bindsym o      mode "$mode_gaps_outer"
            bindsym i      mode "$mode_gaps_inner"
            bindsym r      gaps inner all set 0;           gaps outer all set 0          ; mode "default"
            bindsym d      gaps inner all set $gaps_inner; gaps outer all set $gaps_outer; mode "default"
            bindsym Return mode "default"
            bindsym Escape mode "default"
    }

    mode "$mode_gaps_inner" {
            bindsym plus  gaps inner current plus 5
            bindsym minus gaps inner current minus 5
            bindsym 0     gaps inner current set 0

            bindsym Shift+plus  gaps inner all plus 5
            bindsym Shift+minus gaps inner all minus 5
            bindsym Shift+0     gaps inner all set 0

            bindsym Return mode "default"
            bindsym Escape mode "default"
    }

    mode "$mode_gaps_outer" {
            bindsym plus  gaps outer current plus 5
            bindsym minus gaps outer current minus 5
            bindsym 0     gaps outer current set 0

            bindsym Shift+plus  gaps outer all plus 5
            bindsym Shift+minus gaps outer all minus 5
            bindsym Shift+0     gaps outer all set 0

            bindsym Return mode "default"
            bindsym Escape mode "default"
    }
