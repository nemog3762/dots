 #history is on by default it can be set to -> 0 history off, 1: history on
YTFZF_HIST=1

# if set to 1 it is on but normally it is off by default. Can be turned on using option -l
YTFZF_LOOP=0

# set the video format
YTFZF_PREF="bestvideo[height<=?1080]+bestaudio/best"

# fzf colors are going to be the one from your fzf configuration
YTFZF_ENABLE_FZF_DEFAULT_OPTS=1

# sets the video player used by ytfzf (mpv by default), e.g. FZF_PLAYER="devour mpv"; you can also specify the YTFZF_PLAYER_FORMAT, e.g. YTFZF_PLAYER_FORMAT="devour mpv --ytdl-format="
FZF_PLAYER="mpv"
