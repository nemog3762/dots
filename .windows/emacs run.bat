@echo off

:: Ejecutar Emacs en Windows pero usando el .emacs.d que está ubicado en un directorio específico.

:: Temporalmente se coloca esa ruta como %HOME% y luego se inicia Emacs.
set HOME=D:\dotfiles

:: En caso de error, descomentar la siguiente línea:
:: "C:\Program Files\Emacs\emacs-28.2\bin\runemacs.exe" --debug-init

:: Para funcionamiento normal, ejecutar la siguiente línea:
"C:\Program Files\Emacs\emacs-28.2\bin\runemacs.exe" %

:: Para ver la ejecución del programa, descomentar la siguiente línea.
:: pause
