@echo off

:: Entrar a un repositorio de GIT, descartar los cambios hechos locales y hacer pull de la versión en GIT.

cd D:\dotfiles
git reset --hard
git clean -fd
git pull origin main
pause
