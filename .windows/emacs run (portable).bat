@echo off

:: Colocar este archivo en una carpeta donde están 2 carpetas:
:: - Carpeta (dotfiles) donde dentro está la carpeta .emacs.d
:: - Carpeta de Emacs portable (emacs-28.2)

:: Directorio donde se ubica la carpeta .emacs.d
set HOME=dotfiles

:: Ruta de archivo de Emacs
set FILE=emacs-28.2\bin\runemacs.exe

:: Ejecutar Emacs
%FILE%
