
# Created by `pipx` on 2023-08-06 03:48:26
export PATH="$PATH:/home/user/.local/bin"

# load .bashrc in tmux
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
    . "$HOME/.bashrc"
    fi
fi
alias renombrar_vid_img='for file in VID_* IMG_*; do mv "IMG_20240705_143416328_HDR.jpg" "${file#VID_}" 2>/dev/null || mv "IMG_20240705_143416328_HDR.jpg" "${file#IMG_}"; done'
